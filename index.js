db.users.find({
    $or:[
        {firstName:{$regex:"S",$options: "$i"}},
        {lastName:{$regex:"D",$options: "$i"}}
    ]
});

db.users.find({
    $and:[
        {department:"HR"},
        {age:{$gte:70}}
    ]
});

db.users.find({
    $and:[
        {firstName:{$regex:"e"}},
        {age:{$lte:30}}
    ]
});